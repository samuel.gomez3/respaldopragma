package com.pragma.store.product.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.store.product.entity.Category;
import com.pragma.store.product.entity.Product;
import com.pragma.store.product.repository.ProductRepository;
@Service
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<Product> listAllProduct() {
		// TODO Auto-generated method stub
		return productRepository.findAll();
	}

	@Override
	public Product getProduct(Long id) {
		// TODO Auto-generated method stub
		return productRepository.findById(id).orElse(null);
	}

	@Override
	public Product createProduct(Product product) {
		product.setStatus("CREATED");
		product.setCreateAt(new Date());
		return productRepository.save(product);
	}

	@Override
	public Product updateProduct(Product product) {
		Product productDb = getProduct(product.getId());
		if (null == productDb)
			return null;
		productDb.setName(product.getName());
		productDb.setDescription(product.getDescription());
		productDb.setCategory(product.getCategory());
		productDb.setPrice(product.getPrice());
		return productRepository.save(productDb);
	}

	@Override
	public Product deleteProduct(Long id) {
		Product productDb = getProduct(id);
		if (null == productDb)
			return null;
		productDb.setStatus("DELETED");
		return productRepository.save(productDb);
	}

	@Override
	public List<Product> findByCategory(Category category) {
		// TODO Auto-generated method stub
		return productRepository.findByCategory(category);
	}

	@Override
	public Product updateStock(Long id, Double quantity) {
		Product productDB = getProduct(id);
		if (null == productDB)
			return null;
		productDB.setStock(productDB.getStock() + quantity);
		return productRepository.save(productDB);

	}

}
