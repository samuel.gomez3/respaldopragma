package com.pragma.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.pragma.DemoConsolaApplication;


@Repository
@Qualifier("Persona1")
public class PersonaRepoImpl implements IPersonaRepo {
	private static Logger logger = LoggerFactory.getLogger(DemoConsolaApplication.class);

	@Override
	public void registrar(String nombre) {
		logger.info("Registro1: " + nombre);

	}

}
