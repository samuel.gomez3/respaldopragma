package com.pragma.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.pragma.repository.IPersonaRepo;

@Service
public class PersonaServiceImpl implements IPersonaService {
	@Autowired
	@Qualifier("Persona2")
	private IPersonaRepo iPersonaRepo;

	@Override
	public void registrar(String nombre) {
		iPersonaRepo.registrar(nombre);

	}

}
