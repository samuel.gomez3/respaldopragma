package com.pragma;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.pragma.model.Usuario;
import com.pragma.repo.IUsuarioRepo;

@SpringBootTest
class DemoWeb2ApplicationTests {

	@Autowired
	private IUsuarioRepo repo;
	@Autowired
	private BCryptPasswordEncoder encoder;

	@Test
	void crearUsuario() {
		Usuario usuario = new Usuario();
		usuario.setId(3);
		usuario.setNombre("sami1752");
		usuario.setClave(encoder.encode("1234"));
		Usuario retorno = repo.save(usuario);

		assertTrue(retorno.getClave().equals(usuario.getClave()));
	}

}
