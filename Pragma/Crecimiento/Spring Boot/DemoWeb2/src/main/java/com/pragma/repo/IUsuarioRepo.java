package com.pragma.repo;

import org.springframework.data.jpa.repository.JpaRepository;


import com.pragma.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer> {
	
	Usuario findByNombre(String nombre);
}
