package com.pragma.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pragma.model.Persona;
import com.pragma.repo.IPersonaRepo;

@RestController
@RequestMapping("/personas")
public class RestDemoController {
	@Autowired
	private IPersonaRepo repo;

//	@GetMapping("/listar")
	@GetMapping
	public List<Persona> listar() {
		return repo.findAll();
	}

//	@PostMapping("/insertar")
	@PostMapping
	public void insertar(@RequestBody Persona persona) {
		repo.save(persona);
	}
//	@PutMapping("/modificar")
	@PutMapping
	public void modificar(@RequestBody Persona persona) {
		repo.save(persona);
	}

//	@DeleteMapping("/eliminar/{id}")
	@DeleteMapping("/{id}")
	public void eliminar(@PathVariable("id") int id) {
		repo.deleteById(id);
	}

}
