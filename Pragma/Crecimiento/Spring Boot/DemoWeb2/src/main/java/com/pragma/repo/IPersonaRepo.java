package com.pragma.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.model.Persona;


public interface IPersonaRepo extends JpaRepository<Persona, Integer> {
	
	
}
