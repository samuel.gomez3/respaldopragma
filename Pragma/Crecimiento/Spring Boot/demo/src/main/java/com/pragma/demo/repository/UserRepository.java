package com.pragma.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.pragma.demo.modelo.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	User findByUserName(String name);
}
