package com.pragma.demo.service;

import java.util.List;


import com.pragma.demo.modelo.User;

public interface UserService {

	User findById(long id);

	User findByName(String name);

	void saveUser(User user);

	void updateUser(User user);

	void deleteUser(long id);

	void deleteAllUser();

	List<User> findAllUsers();

	public boolean isUserExist(User user);

}
