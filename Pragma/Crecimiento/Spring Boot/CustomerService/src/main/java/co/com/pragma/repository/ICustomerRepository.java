package co.com.pragma.repository;

import org.springframework.data.repository.CrudRepository;

import co.com.pragma.models.Customer;

public interface ICustomerRepository extends CrudRepository<Customer, Long> {

	Customer findByIdentificationNumber(String identificationNumber);

}
