package co.com.pragma.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.pragma.models.Customer;
import co.com.pragma.repository.ICustomerRepository;
import co.com.pragma.service.ICustomerService;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private ICustomerRepository customerRepository;

	@Override
	public Customer findCustomerById(long id) {
		return (Customer) customerRepository.findById(id).orElse(null);
	}

	@Override
	public Customer findCustomerByIdentificationNumber(String identificationNumber) {
		if (identificationNumber.matches("[+-]?\\d*(\\.\\d+)?"))
			return customerRepository.findByIdentificationNumber(identificationNumber);
		else
			return customerRepository.findByIdentificationNumber(identificationNumber.substring(2));
	}

	@Override
	public void saveCustomer(Customer customer) {
		customerRepository.save(customer);
	}

	@Override
	public void updateCustomer(Customer customer) {
		customerRepository.save(customer);
	}

	@Override
	public void deleteCustomer(long id) {
		customerRepository.deleteById(id);
	}

	@Override
	public List<Customer> findAllCustomers() {
		return (List<Customer>) customerRepository.findAll();
	}

	@Override
	public boolean isExist(Customer customer) {
		return findCustomerByIdentificationNumber(customer.getIdentificationNumber()) != null;
	}

	@Override
	public List<Customer> findCustomersByAge(int age) {
		return ((List<Customer>) customerRepository.findAll()).stream().filter(p -> p.getAge() >= age).toList();
	}

}
