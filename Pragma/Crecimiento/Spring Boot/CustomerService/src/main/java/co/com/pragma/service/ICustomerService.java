package co.com.pragma.service;

import java.util.List;

import co.com.pragma.models.Customer;

public interface ICustomerService {
	Customer findCustomerById(long id);

	Customer findCustomerByIdentificationNumber(String identificationNumber);

	void saveCustomer(Customer customer);

	void updateCustomer(Customer customer);

	void deleteCustomer(long id);

	List<Customer> findAllCustomers();

	public boolean isExist(Customer customer);

	List<Customer> findCustomersByAge(int age);

}
