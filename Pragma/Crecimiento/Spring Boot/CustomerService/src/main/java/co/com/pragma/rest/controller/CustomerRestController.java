package co.com.pragma.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.pragma.models.Customer;
import co.com.pragma.service.ICustomerService;

@RestController
@RequestMapping("/customers")
@CrossOrigin(origins = "*", methods = { RequestMethod.POST, RequestMethod.GET, RequestMethod.PUT,
		RequestMethod.DELETE })
public class CustomerRestController {

	@Autowired
	private ICustomerService customerService;

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Customer>> listAllCustomers() {
		List<Customer> clientes = customerService.findAllCustomers();
		if (clientes.isEmpty())
			return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Customer>>(clientes, HttpStatus.OK);
	}

	@RequestMapping(value = "/age/{age}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Customer>> listCustomersByAge(@PathVariable("age") int age) {
		List<Customer> customers = customerService.findCustomersByAge(age);
		if (customers.isEmpty())
			return new ResponseEntity<List<Customer>>(HttpStatus.NO_CONTENT);
		return new ResponseEntity<List<Customer>>(customers, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Customer> getCustomerById(@PathVariable("id") long id) {
		Customer customer = customerService.findCustomerById(id);
		if (customer == null)
			return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	@RequestMapping(value = "/identificationNumber/{identificationNumber}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Customer> getCustomerByIdentificationNumber(
			@PathVariable("identificationNumber") String identificationNumber) {
		Customer customer = customerService.findCustomerByIdentificationNumber(identificationNumber);
		if (customer == null)
			return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public ResponseEntity<Void> createCustomer(@Valid @RequestBody Customer customer, BindingResult result,
			UriComponentsBuilder uriComponentsBuilder) {
		if (result.hasErrors())
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, this.formatMessage(result));
		if (customerService.isExist(customer))
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		customerService.saveCustomer(customer);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/{id}").buildAndExpand(customer.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Customer> updateCustomer(@PathVariable("id") long id, @RequestBody Customer customer) {
		Customer currentCustomer = customerService.findCustomerById(id);
		if (currentCustomer == null)
			return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		currentCustomer.setSurnames(customer.getSurnames());
		currentCustomer.setCityBirth(customer.getCityBirth());
		currentCustomer.setAge(customer.getAge());
		currentCustomer.setNames(customer.getNames());
		currentCustomer.setIdentificationNumber(customer.getIdentificationNumber());
		currentCustomer.setTypeIdentification(customer.getTypeIdentification());
		customerService.updateCustomer(currentCustomer);
		return new ResponseEntity<Customer>(currentCustomer, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Customer> deleteCustomer(@PathVariable("id") long id) {
		Customer cliente = customerService.findCustomerById(id);
		if (cliente == null)
			return new ResponseEntity<Customer>(HttpStatus.NOT_FOUND);
		customerService.deleteCustomer(id);
		return new ResponseEntity<Customer>(HttpStatus.NO_CONTENT);
	}

	private String formatMessage(BindingResult result) {
		List<Map<String, String>> errors = result.getFieldErrors().stream().map(err -> {
			Map<String, String> error = new HashMap<>();
			error.put(err.getField(), err.getDefaultMessage());
			return error;
		}).collect(Collectors.toList());
		ErrorMessage errorMessage = ErrorMessage.builder().code("01").messages(errors).build();
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = mapper.writeValueAsString(errorMessage);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonString;
	}

}
